
// let url = encodeURIComponent(`https://www.amazon.com/s?k=porn`);
// http://api.proxycrawl.com/?token=${token}&scraper=amazon-serp&url=${url}
// [{response:{}, list:[{},{}], key:"String"},{response:{}, list:[{},{}], key:"String"},]
let products = [
    {
        response: {
            original_status: 200,
            pc_status: 200,
            url: "https://www.amazon.com/s?k=games",
            body: {
                products: [
                    {
                        name: "WHAT DO YOU MEME? Party Game",
                        price: "$29.99",
                        customerReview: "4.6 out of 5 stars",
                        customerReviewCount: "8,236",
                        shippingMessage: "Get it as soon as Tue, Jan 28 FREE Shipping by Amazon",
                        asin: "B01MRG7T0D",
                        image: "https://m.media-amazon.com/images/I/516jTn7V97L._AC_UY218_ML3_.jpg",
                        url: "https://www.amazon.com/What-Do-You-Meme-Party/dp/B01MRG7T0D/ref=sr_1_1?keywords=games&qid=1579978609&sr=8-1",
                        isPrime: true
                    },
                    {
                        name: "Unstable Unicorns Card Game - A Strategic Card Game & Party Game for Adults & Teens (2ND Edition with New Cards!)",
                        price: "$20.00",
                        customerReview: "4.6 out of 5 stars",
                        customerReviewCount: "2,878",
                        shippingMessage: "Get it as soon as Wed, Jan 29 FREE Shipping on orders over $25 shipped by Amazon",
                        asin: "B07V3NH51J",
                        image: "https://m.media-amazon.com/images/I/51o4HJhyJrL._AC_UY218_ML3_.jpg",
                        url: "https://www.amazon.com/Unstable-Unicorns-Card-Game-Strategic/dp/B07V3NH51J/ref=sr_1_2?keywords=games&qid=1579978609&sr=8-2",
                        isPrime: true
                    },
                    {
                        name: "PlayMonster 5 Second Rule Game - New Edition",
                        price: "$15.99",
                        customerReview: "4.6 out of 5 stars",
                        customerReviewCount: "3,375",
                        shippingMessage: "Get it as soon as Wed, Jan 29 FREE Shipping on orders over $25 shipped by Amazon",
                        asin: "B07D5QHKJN",
                        image: "https://m.media-amazon.com/images/I/81ya9BxaovL._AC_UY218_ML3_.jpg",
                        url: "https://www.amazon.com/PlayMonster-Second-Rule-Game-NEW/dp/B07D5QHKJN/ref=sr_1_3?keywords=games&qid=1579978609&sr=8-3",
                        isPrime: true
                    },
                    {
                        name: "Exploding Kittens Card Game - Family-Friendly Party Games - Card Games For Adults, Teens & Kids",
                        price: "$19.99",
                        customerReview: "4.7 out of 5 stars",
                        customerReviewCount: "13,457",
                        shippingMessage: "Get it as soon as Wed, Jan 29 FREE Shipping on orders over $25 shipped by Amazon",
                        asin: "B010TQY7A8",
                        image: "https://m.media-amazon.com/images/I/81gF5y9IT3L._AC_UY218_ML3_.jpg",
                        url: "https://www.amazon.com/Exploding-Kittens-LLC-EKG-ORG1-1-Card/dp/B010TQY7A8/ref=sr_1_4?keywords=games&qid=1579978609&sr=8-4",
                        isPrime: true
                    },
                    {
                        name: "New Phone, Who Dis? - Adult Party Game by What Do You Meme?",
                        price: "$19.99",
                        customerReview: "4.4 out of 5 stars",
                        customerReviewCount: "1,536",
                        shippingMessage: "Get it as soon as Wed, Jan 29 FREE Shipping on orders over $25 shipped by Amazon",
                        asin: "B07PZF5C2J",
                        image: "https://m.media-amazon.com/images/I/71cdOZhGBwL._AC_UY218_ML3_.jpg",
                        url: "https://www.amazon.com/New-Phone-Who-Dis-Adult/dp/B07PZF5C2J/ref=sr_1_5?keywords=games&qid=1579978609&sr=8-5",
                        isPrime: true
                    },
                    {
                        name: "Mattel Games UNO: Classic (Tin Box) [Amazon Exclusive]",
                        price: "$9.86",
                        customerReview: "4.8 out of 5 stars",
                        customerReviewCount: "5,002",
                        shippingMessage: "Get it as soon as Wed, Jan 29 FREE Shipping on orders over $25 shipped by Amazon",
                        asin: "B07P6MZPK3",
                        image: "https://m.media-amazon.com/images/I/81TX8WTjiuL._AC_UY218_ML3_.jpg",
                        url: "https://www.amazon.com/Mattel-Games-Official-Amazon-Exclusive/dp/B07P6MZPK3/ref=ice_ac_b_dpb?keywords=games&qid=1579978609&sr=8-6",
                        isPrime: true
                    },
                    {
                        name: "Catch Phrase Game, Frustration-Free Packaging",
                        price: "$13.99",
                        customerReview: "4.7 out of 5 stars",
                        customerReviewCount: "1,438",
                        shippingMessage: "Get it as soon as Thu, Jan 30 FREE Shipping on orders over $25 shipped by Amazon",
                        asin: "B01BOV6566",
                        image: "https://m.media-amazon.com/images/I/91aj+0G6R0L._AC_UY218_ML3_.jpg",
                        url: "https://www.amazon.com/gp/bestsellers/toys-and-games/166179011/ref=sr_bs_6_166179011_1",
                        isPrime: true
                    },
                    {
                        name: "Not Parent Approved: A Card Game for Kids, Families and Mischief Makers",
                        price: "$24.99",
                        customerReview: "4.6 out of 5 stars",
                        customerReviewCount: "1,833",
                        shippingMessage: "Get it as soon as Wed, Jan 29 FREE Shipping on orders over $25 shipped by Amazon",
                        asin: "B01FST8A90",
                        image: "https://m.media-amazon.com/images/I/81N-g8wMseL._AC_UY218_ML3_.jpg",
                        url: "https://www.amazon.com/Not-Parent-Approved-Families-Mischief/dp/B01FST8A90/ref=sr_1_8?keywords=games&qid=1579978609&sr=8-8",
                        isPrime: true
                    },
                    {
                        name: "WHAT DO YOU MEME? Incohearent - Adult Party Game",
                        price: "$19.99",
                        customerReview: "4.7 out of 5 stars",
                        customerReviewCount: "556",
                        shippingMessage: "Get it as soon as Wed, Jan 29 FREE Shipping on orders over $25 shipped by Amazon",
                        asin: "B07QSCQMXV",
                        image: "https://m.media-amazon.com/images/I/71DuNDg2-LL._AC_UY218_ML3_.jpg",
                        url: "https://www.amazon.com/gp/bestsellers/toys-and-games/166239011/ref=sr_bs_8_166239011_1",
                        isPrime: true
                    },
                    {
                        name: "Cards Against Humanity",
                        price: "$25.00",
                        customerReview: "4.8 out of 5 stars",
                        customerReviewCount: "38,519",
                        shippingMessage: "Get it as soon as Tue, Jan 28 FREE Shipping by Amazon",
                        asin: "B004S8F7QM",
                        image: "https://m.media-amazon.com/images/I/71LpuvHDRLL._AC_UY218_ML3_.jpg",
                        url: "https://www.amazon.com/Cards-Against-Humanity-LLC-CAHUS/dp/B004S8F7QM/ref=sr_1_10?keywords=games&qid=1579978609&sr=8-10",
                        isPrime: true
                    },
                    {
                        name: "Spontuneous - The Song Game - Sing It or Shout It - Talent NOT Required (Family / Party Board Game)",
                        price: "$29.99",
                        customerReview: "4.5 out of 5 stars",
                        customerReviewCount: "2,724",
                        shippingMessage: "Get it as soon as Tue, Jan 28 FREE Shipping by Amazon",
                        asin: "B004LO2AIG",
                        image: "https://m.media-amazon.com/images/I/71k8b0yo3UL._AC_UY218_ML3_.jpg",
                        url: "https://www.amazon.com/Spontuneous-Talent-Required-Family-Adults/dp/B004LO2AIG/ref=sr_1_11?keywords=games&qid=1579978609&sr=8-11",
                        isPrime: true
                    },
                    {
                        name: "These Cards Will Get You Drunk - Fun Adult Drinking Game for Parties",
                        price: "$15.99",
                        customerReview: "4.6 out of 5 stars",
                        customerReviewCount: "1,590",
                        shippingMessage: "Get it as soon as Wed, Jan 29 FREE Shipping on orders over $25 shipped by Amazon",
                        asin: "B073R59XYF",
                        image: "https://m.media-amazon.com/images/I/61OJxs1k5WL._AC_UY218_ML3_.jpg",
                        url: "https://www.amazon.com/These-Cards-Will-Get-Drunk/dp/B073R59XYF/ref=sr_1_12?keywords=games&qid=1579978609&sr=8-12",
                        isPrime: true
                    },
                    {
                        name: "Jenga Classic Game",
                        price: null,
                        customerReview: "4.8 out of 5 stars",
                        customerReviewCount: "7,709",
                        shippingMessage: "Get it as soon as Wed, Jan 29 FREE Shipping on orders over $25 shipped by Amazon",
                        asin: "B00ABA0ZOA",
                        image: "https://m.media-amazon.com/images/I/81OAWwX3djL._AC_UY218_ML3_.jpg",
                        url: "https://www.amazon.com/gp/bestsellers/toys-and-games/166263011/ref=sr_bs_12_166263011_1",
                        isPrime: true
                    },
                    {
                        name: "Game of Thrones Season 1",
                        price: "$2.99",
                        customerReview: "4.5 out of 5 stars",
                        customerReviewCount: "7,801",
                        shippingMessage: null,
                        asin: "B007HJAJSA",
                        image: "https://m.media-amazon.com/images/I/91iMNOfH4YL._AC_UY218_ML3_.jpg",
                        url: "https://www.amazon.com/Baelor/dp/B007BVOEPI/ref=sr_1_14?keywords=games&qid=1579978609&sr=8-14",
                        isPrime: false
                    },
                    {
                        name: "That's What She Said - The Party Game of Twisted Innuendos",
                        price: "$24.95",
                        customerReview: "4.7 out of 5 stars",
                        customerReviewCount: "2,333",
                        shippingMessage: "Get it as soon as Wed, Jan 29 FREE Shipping on orders over $25 shipped by Amazon",
                        asin: "B01M4S54JJ",
                        image: "https://m.media-amazon.com/images/I/81CsqsGVRDL._AC_UY218_ML3_.jpg",
                        url: "https://www.amazon.com/Thats-What-She-Said-Innuendos/dp/B01M4S54JJ/ref=sr_1_15?keywords=games&qid=1579978609&sr=8-15",
                        isPrime: true
                    },
                    {
                        name: "Clue Game",
                        price: "$7.88",
                        customerReview: "4.8 out of 5 stars",
                        customerReviewCount: "1,925",
                        shippingMessage: "Get it as soon as Wed, Jan 29 FREE Shipping on orders over $25 shipped by Amazon",
                        asin: "B01JYVHMVA",
                        image: "https://m.media-amazon.com/images/I/91LmzJhfuZL._AC_UY218_ML3_.jpg",
                        url: "https://www.amazon.com/Hasbro-A5826079-Clue-Game/dp/B01JYVHMVA/ref=sr_1_16?keywords=games&qid=1579978609&sr=8-16",
                        isPrime: true
                    }
                ],
                pagination: {
                    currentPage: 1,
                    nextPage: 2
                }
            }
        },
        "list": [
            {
                "name": "Amazon's Choice Customers shopped Amazon's Choice for…",
                "price": "$20.00",
                "customerReview": "4.6 out of 5 stars",
                "customerReviewCount": "2,878",
                "shippingMessage": null,
                "asin": "",
                "image": "https://m.media-amazon.com/images/I/51o4HJhyJrL._AC_UL320_ML3_.jpg",
                "url": "https://www.amazon.com/Unstable-Unicorns-Card-Game-Strategic/dp/B07V3NH51J/ref=sxin_0_ac_d_rm?ac_md=0-0-Z2FtZQ%3D%3D-ac_d_rm&cv_ct_cx=game&keywords=game&pd_rd_i=B07V3NH51J&pd_rd_r=10221846-fb83-4fb5-98cc-cfd9750c2056&pd_rd_w=3WehP&pd_rd_wg=D6ScL&pf_rd_p=e2f20af2-9651-42af-9a45-89425d5bae34&pf_rd_r=XY8TCSNAF1VFZY4FTVP8&psc=1&qid=1579985165&sr=1-1-12d4272d-8adb-4121-8624-135149aa9081",
                "isPrime": true,
                "details": [
                    {
                        "name": null,
                        "price": null,
                        "canonicalUrl": null,
                        "isPrime": false,
                        "inStock": false,
                        "mainImage": null,
                        "images": null,
                        "customerReview": "",
                        "customerReviewCount": null,
                        "shippingMessage": null,
                        "features": [],
                        "description": "",
                        "breadCrumbs": [],
                        "productInformation": []
                    }
                ]
            },
            {
                "name": "Editorial recommendations",
                "price": "$25.00",
                "customerReview": "4.8 out of 5 stars",
                "customerReviewCount": "38,519",
                "shippingMessage": null,
                "asin": "",
                "image": "https://m.media-amazon.com/images/I/71LpuvHDRLL._AC_UL320_ML3_.jpg",
                "url": "https://www.amazon.com/gp/profile/amzn1.account.AHCXDMVI2QGOF7XAXX3SB5UI6AEA/ref=sxin_1_osp48-65351ba5_cov?pd_rd_w=mynLB&pf_rd_p=eb3e5cda-5ec9-4d94-919d-310a5d641b8b&pf_rd_r=XY8TCSNAF1VFZY4FTVP8&pd_rd_r=10221846-fb83-4fb5-98cc-cfd9750c2056&pd_rd_wg=D6ScL&cv_ct_pg=search&cv_ct_wn=osp-search&ascsubtag=65351ba5-a53b-4f39-bc09-fd8b9f6bb349&linkCode=oas&cv_ct_id=amzn1.osp.65351ba5-a53b-4f39-bc09-fd8b9f6bb349&tag=spyonsite-20&qid=1579985165&cv_ct_cx=game",
                "isPrime": true,
                "details": [
                    {
                        "name": null,
                        "price": null,
                        "canonicalUrl": null,
                        "isPrime": false,
                        "inStock": false,
                        "mainImage": null,
                        "images": null,
                        "customerReview": "",
                        "customerReviewCount": null,
                        "shippingMessage": null,
                        "features": [],
                        "description": "",
                        "breadCrumbs": [],
                        "productInformation": []
                    }
                ]
            },
            {
                "name": "Game of Thrones Season 1",
                "price": "$2.99",
                "customerReview": "4.5 out of 5 stars",
                "customerReviewCount": "7,801",
                "shippingMessage": null,
                "asin": "B007HJ8RTI",
                "image": "https://m.media-amazon.com/images/I/91iMNOfH4YL._AC_UY218_ML3_.jpg",
                "url": "https://www.amazon.com/The-Kingsroad/dp/B007BVOEPI/ref=sr_1_1?keywords=game&qid=1579985165&sr=8-1",
                "isPrime": false,
                "details": [
                    {
                        "name": null,
                        "price": null,
                        "canonicalUrl": "https://www.amazon.com/Game-of-Thrones-Season-1/dp/B007BVMVDU",
                        "isPrime": false,
                        "inStock": false,
                        "mainImage": null,
                        "images": null,
                        "customerReview": "4.5 out of 5",
                        "customerReviewCount": "7",
                        "shippingMessage": null,
                        "features": [],
                        "description": "",
                        "breadCrumbs": [],
                        "productInformation": []
                    }
                ]
            },
            {
                "name": "Game of Thrones Season 2",
                "price": "$2.99",
                "customerReview": "4.7 out of 5 stars",
                "customerReviewCount": "5,326",
                "shippingMessage": null,
                "asin": "B00BHAT2AQ",
                "image": "https://m.media-amazon.com/images/I/91z6bOFnvaL._AC_UY218_ML3_.jpg",
                "url": "https://www.amazon.com/The-North-Remembers/dp/B00AN0KGWE/ref=sr_1_2?keywords=game&qid=1579985165&sr=8-2",
                "isPrime": false,
                "details": [
                    {
                        "name": null,
                        "price": null,
                        "canonicalUrl": "https://www.amazon.com/Game-of-Thrones-Season-2/dp/B00AN0KBJC",
                        "isPrime": false,
                        "inStock": false,
                        "mainImage": null,
                        "images": null,
                        "customerReview": "4.7 out of 5",
                        "customerReviewCount": "5",
                        "shippingMessage": null,
                        "features": [],
                        "description": "",
                        "breadCrumbs": [],
                        "productInformation": []
                    }
                ]
            },
            {
                "name": "Game of Thrones - Season 8",
                "price": "$0.00",
                "customerReview": "3.8 out of 5 stars",
                "customerReviewCount": "645",
                "shippingMessage": null,
                "asin": "B07S2DWTYM",
                "image": "https://m.media-amazon.com/images/I/91L8bmPKClL._AC_UY218_ML3_.jpg",
                "url": "https://www.amazon.com/Season-8-Trailer/dp/B07KGL9531/ref=sr_1_3?keywords=game&qid=1579985165&sr=8-3",
                "isPrime": false,
                "details": [
                    {
                        "name": null,
                        "price": null,
                        "canonicalUrl": "https://www.amazon.com/Game-of-Thrones-Season-8/dp/B07KGL9531",
                        "isPrime": false,
                        "inStock": false,
                        "mainImage": null,
                        "images": null,
                        "customerReview": "3.8 out of 5",
                        "customerReviewCount": "645",
                        "shippingMessage": null,
                        "features": [],
                        "description": "",
                        "breadCrumbs": [],
                        "productInformation": []
                    }
                ]
            },
            {
                "name": "Game of Thrones Season 5",
                "price": "$2.99",
                "customerReview": "4.8 out of 5 stars",
                "customerReviewCount": "4,251",
                "shippingMessage": null,
                "asin": "B014OK2Y78",
                "image": "https://m.media-amazon.com/images/I/913pQH48dOL._AC_UY218_ML3_.jpg",
                "url": "https://www.amazon.com/The-Wars-To-Come/dp/B00ZOSV5HU/ref=sr_1_4?keywords=game&qid=1579985165&sr=8-4",
                "isPrime": false
            },
            {
                "name": "Recommended Articles",
                "price": null,
                "customerReview": null,
                "customerReviewCount": null,
                "shippingMessage": null,
                "asin": "",
                "image": null,
                "url": "https://www.amazon.com/ospublishing/story/49c65766-8070-4f1a-9e2f-893a2b6018e7/ref=sxin_6?pd_rd_w=ZXyN5&pf_rd_p=ce108cca-d6c6-4ed4-a7cb-ae1ed3151224&pf_rd_r=XY8TCSNAF1VFZY4FTVP8&pd_rd_r=10221846-fb83-4fb5-98cc-cfd9750c2056&pd_rd_wg=D6ScL&qid=1579985165&cv_ct_pg=search&cv_ct_wn=osp-multisource&ascsubtag=amzn1.osa.49c65766-8070-4f1a-9e2f-893a2b6018e7.ATVPDKIKX0DER.en_US&linkCode=oas&cv_ct_id=amzn1.osa.49c65766-8070-4f1a-9e2f-893a2b6018e7.ATVPDKIKX0DER.en_US&tag=theradar-20&cv_ct_cx=game",
                "isPrime": false
            },
            {
                "name": "Game of Thrones Season 4",
                "price": "$2.99",
                "customerReview": "4.7 out of 5 stars",
                "customerReviewCount": "4,830",
                "shippingMessage": null,
                "asin": "B00TOBG9B0",
                "image": "https://m.media-amazon.com/images/I/915tMZ56VVL._AC_UY218_ML3_.jpg",
                "url": "https://www.amazon.com/Two-Swords/dp/B00PG3KMBS/ref=sr_1_5?keywords=game&qid=1579985165&sr=8-5",
                "isPrime": false
            },
            {
                "name": "Unstable Unicorns Card Game - A Strategic Card Game & Party Game for Adults & Teens (2ND Edition with New Cards!)",
                "price": "$20.00",
                "customerReview": "4.6 out of 5 stars",
                "customerReviewCount": "2,878",
                "shippingMessage": "Get it as soon as Wed, Jan 29 FREE Shipping on orders over $25 shipped by Amazon",
                "asin": "B07V3NH51J",
                "image": "https://m.media-amazon.com/images/I/51o4HJhyJrL._AC_UY218_ML3_.jpg",
                "url": "https://www.amazon.com/Unstable-Unicorns-Card-Game-Strategic/dp/B07V3NH51J/ref=ice_ac_b_dpb?keywords=game&qid=1579985165&sr=8-6",
                "isPrime": true
            },
            {
                "name": "Game of Thrones: Season 6",
                "price": "$2.99",
                "customerReview": "4.7 out of 5 stars",
                "customerReviewCount": "3,960",
                "shippingMessage": null,
                "asin": "B01J4L7BWU",
                "image": "https://m.media-amazon.com/images/I/A1P+Wxkkb7L._AC_UY218_ML3_.jpg",
                "url": "https://www.amazon.com/The-Winds-of-Winter/dp/B01HKAXUJU/ref=sr_1_7?keywords=game&qid=1579985165&sr=8-7",
                "isPrime": false
            },
            {
                "name": "Game of Thrones Season 3",
                "price": "$2.99",
                "customerReview": "4.8 out of 5 stars",
                "customerReviewCount": "2,080",
                "shippingMessage": null,
                "asin": "B00HXNFQJW",
                "image": "https://m.media-amazon.com/images/I/81zMarLIZhL._AC_UY218_ML3_.jpg",
                "url": "https://www.amazon.com/Second-Sons/dp/B00HV29XAI/ref=sr_1_8?keywords=game&qid=1579985165&sr=8-8",
                "isPrime": false
            },
            {
                "name": "Game Of Thrones - Season 7",
                "price": "$2.99",
                "customerReview": "4.7 out of 5 stars",
                "customerReviewCount": "2,263",
                "shippingMessage": null,
                "asin": "B0755N8DQ3",
                "image": "https://m.media-amazon.com/images/I/91KwasehsHL._AC_UY218_ML3_.jpg",
                "url": "https://www.amazon.com/The-Dragon-and-the-Wolf/dp/B06XNSRC1Z/ref=sr_1_9?keywords=game&qid=1579985165&sr=8-9",
                "isPrime": false
            },
            {
                "name": "Cooking City: crazy chef’ s restaurant game",
                "price": null,
                "customerReview": "4.0 out of 5 stars",
                "customerReviewCount": "65",
                "shippingMessage": null,
                "asin": "B081VGCVGJ",
                "image": "https://m.media-amazon.com/images/I/811FCR+KFRL._AC_UY218_ML3_.png",
                "url": "https://www.amazon.com/Cooking-City-crazy-chef-restaurant/dp/B081VGCVGJ/ref=sr_1_10?keywords=game&qid=1579985165&sr=8-10",
                "isPrime": false
            },
            {
                "name": "The Game",
                "price": "$3.99",
                "customerReview": "4.6 out of 5 stars",
                "customerReviewCount": "967",
                "shippingMessage": null,
                "asin": "B000I9TY5U",
                "image": "https://m.media-amazon.com/images/I/718yQn5UMOL._AC_UY218_ML3_.jpg",
                "url": "https://www.amazon.com/Game-Michael-Douglas/dp/B000I9TY5U/ref=sr_1_11?keywords=game&qid=1579985165&sr=8-11",
                "isPrime": false
            },
            {
                "name": "PlayMonster 5 Second Rule Game - New Edition",
                "price": "$15.99",
                "customerReview": "4.6 out of 5 stars",
                "customerReviewCount": "3,375",
                "shippingMessage": "Get it as soon as Wed, Jan 29 FREE Shipping on orders over $25 shipped by Amazon",
                "asin": "B07D5QHKJN",
                "image": "https://m.media-amazon.com/images/I/81ya9BxaovL._AC_UY218_ML3_.jpg",
                "url": "https://www.amazon.com/PlayMonster-Second-Rule-Game-NEW/dp/B07D5QHKJN/ref=sr_1_12?keywords=game&qid=1579985165&sr=8-12",
                "isPrime": true
            },
            {
                "name": "WipeOut Fun Race!",
                "price": null,
                "customerReview": "3.4 out of 5 stars",
                "customerReviewCount": "131",
                "shippingMessage": null,
                "asin": "B07TY1C324",
                "image": "https://m.media-amazon.com/images/I/71+58usmVHL._AC_UY218_ML3_.png",
                "url": "https://www.amazon.com/GameArtsy-WipeOut-Fun-Race/dp/B07TY1C324/ref=sr_1_13?keywords=game&qid=1579985165&sr=8-13",
                "isPrime": false
            },
            {
                "name": "Game Changers",
                "price": "$3.99",
                "customerReview": "4.9 out of 5 stars",
                "customerReviewCount": "14",
                "shippingMessage": null,
                "asin": "B07F1PXS8W",
                "image": "https://m.media-amazon.com/images/I/81Ch1baObuL._AC_UY218_ML3_.jpg",
                "url": "https://www.amazon.com/Game-Changers-Alex-Trebek/dp/B07F1PXS8W/ref=sr_1_14?keywords=game&qid=1579985165&sr=8-14",
                "isPrime": false
            },
            {
                "name": "Exploding Kittens Card Game - Family-Friendly Party Games - Card Games For Adults, Teens & Kids",
                "price": "$19.99",
                "customerReview": "4.7 out of 5 stars",
                "customerReviewCount": "13,457",
                "shippingMessage": "Get it as soon as Wed, Jan 29 FREE Shipping on orders over $25 shipped by Amazon",
                "asin": "B010TQY7A8",
                "image": "https://m.media-amazon.com/images/I/81gF5y9IT3L._AC_UY218_ML3_.jpg",
                "url": "https://www.amazon.com/Exploding-Kittens-LLC-EKG-ORG1-1-Card/dp/B010TQY7A8/ref=sr_1_15?keywords=game&qid=1579985165&sr=8-15",
                "isPrime": true
            },
            {
                "name": "WHAT DO YOU MEME? Party Game",
                "price": "$29.99",
                "customerReview": "4.6 out of 5 stars",
                "customerReviewCount": "8,236",
                "shippingMessage": "Get it as soon as Tue, Jan 28 FREE Shipping by Amazon",
                "asin": "B01MRG7T0D",
                "image": "https://m.media-amazon.com/images/I/516jTn7V97L._AC_UY218_ML3_.jpg",
                "url": "https://www.amazon.com/What-Do-You-Meme-Party/dp/B01MRG7T0D/ref=sr_1_16?keywords=game&qid=1579985165&sr=8-16",
                "isPrime": true
            }
        ],
        keyword: "games"
    }
]

// response from productDetailsWithKeyAndAsin
//amazon-product-details
let reposnse_details = {
        "original_status": 200,
        "pc_status": 200,
        "url": "https://www.amazon.com/Apple-MacBook-13-inch-256GB-Storage/dp/B07S5QWM6L",
        "body": {
            "name": "New Apple MacBook Pro (13-inch, 8GB RAM, 256GB Storage) - Silver",
            "price": "$1,599.99",
            "canonicalUrl": "https://www.amazon.com/Apple-MacBook-13-inch-256GB-Storage/dp/B07S5QWM6L",
            "isPrime": true,
            "inStock": true,
            "mainImage": "https://images-na.ssl-images-amazon.com/images/I/518VAg5uOQL.jpg",
            "images": [
                "https://images-na.ssl-images-amazon.com/images/I/31Pag8jpBDL.jpg",
                "https://images-na.ssl-images-amazon.com/images/I/51T9hGgQYnL.jpg",
                "https://images-na.ssl-images-amazon.com/images/I/31hww15IxjL.jpg",
                "https://images-na.ssl-images-amazon.com/images/I/21TPHapZkiL.jpg",
                "https://images-na.ssl-images-amazon.com/images/I/31svYqglKmL.jpg"
            ],
            "customerReview": "4.5 out of 5 stars",
            "customerReviewCount": "446",
            "shippingMessage": "FREE Shipping",
            "features": [
                "8Th-generation quad-core Intel Core i5 Processor",
                "Brilliant Retina Display with True Tone technology",
                "Touch Bar and Touch ID",
                "Intel Iris Plus Graphics 655",
                "Ultrafast SSD",
                "Four Thunderbolt 3 (USB-C) ports",
                "Up to 10 hours of battery life",
                "802.11AC Wi-Fi",
                "Latest Apple-designed keyboard",
                "Force touch trackpad"
            ],
            "description": "",
            "breadCrumbs": [],
            "productInformation": [
                {
                    "name": "Display",
                    "value": "13.3-inch (diagonal) LED-backlit Retina display with IPS technology; 2560-by-1600 native resolution at 227 pixels per inch with support for millions of colors, 16:10 aspect ratio"
                },
                {
                    "name": "Processor",
                    "value": "2.4GHz quad-core Intel Core i5, Turbo Boost up to 4.1GHz, with 128MB of eDRAM"
                },
                {
                    "name": "Graphics and Video Support",
                    "value": "Intel Iris Plus Graphics 655"
                },
                {
                    "name": "Charging and Expansion",
                    "value": "Four Thunderbolt 3 (USB-C) ports with support for: Charging, DisplayPort; Thunderbolt (up to 40 Gbps), USB 3.1 Gen 2 (up to 10 Gbps)"
                },
                {
                    "name": "Wireless",
                    "value": "Wi-Fi; 802.11ac Wi-Fi wireless networking; IEEE 802.11a/b/g/n compatible; Bluetooth 5.0 wireless technology"
                },
                {
                    "name": "In the Box",
                    "value": "13-inch MacBook Pro, 61W USB-C Power Adapter, USB-C Charge Cable (2 m)"
                },
                {
                    "name": "Height",
                    "value": "0.59 inch (1.49 cm)"
                },
                {
                    "name": "Width",
                    "value": "11.97 inches (30.41 cm)"
                },
                {
                    "name": "Depth",
                    "value": "8.36 inches (21.24 cm)"
                },
                {
                    "name": "Weight",
                    "value": "3.02 pounds (1.37 kg)"
                },
                {
                    "name": "Release Date",
                    "value": "5/21/2019"
                }
            ]
        }
}

let addTemplateObj = {
    'templateName': "Template Name",
    'postTitle': "Post Title",
    'postContent': "<h1><%=product_name_1%></h1><h2><em>Hello there</em></h2>",
    'templateCategory': "Top & Best",
}
/**
 * TODO
 * change ooject as the below one
 */
// 'product_name_1': "Template Name",
//         'product_price_1': "Post Title",
//         'product_link_1': "https://jsfiddle.net/40zpau8v/",
//         'product_image_1': "Top & Best",
//         'keyword' : "I phone"

module.exports = {products, addTemplateObj}