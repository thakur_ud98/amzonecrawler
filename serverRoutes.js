const UserRoute = {
    /**Will be used as
     * /user/template
     */
    templateList : '/template',
    addTemplate : '/template'
}

module.exports = {UserRoute}