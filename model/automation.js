const mongoose = require('mongoose')
// const validator = require('validator')
// const bcrypt = require('bcryptjs')
// const jwt = require('jsonwebtoken')

const automationSchema = new mongoose.Schema({
    keywords: {
        type: String,
        required: true,
        trim: true
    },
    site: {
        siteName: {
            type: String,
            required: true,
            trim: true
        },
        siteUrl: {
            type: String,
            required: true,
            trim: true,
        },
        siteUserName: {
            type: String,
            required: true,
            trim: true,
        },
        sitePassword: {
            type: String,
            required: true,
            trim: true
        }
    },
    template: {
        postTitle: {
            type: String,
            required: true,
            trim: true,
        },
        templateName: {
            type: String,
            required: true,
            trim: true,
        },
        postContent: {
            type: String,
            required: true,
            trim: true,
        },
        templateCategory: {
            type: String,
            required: true,
            trim: true,
        }
    },
    status: {
        type: String,
        enum: ['Running', 'Error', 'Completed'],
        default: 'Running'
    },
    pushed: {
        type: Boolean,
        enum: [true, false],
        default: false
    },
    owner:{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },

}, { collection: 'automation', timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } })




const Automation = mongoose.model("Automation", automationSchema)

module.exports = Automation