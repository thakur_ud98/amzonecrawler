const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const Template = require('./template')
const Automation = require('./automation')
const config = require('../config')

const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
        trim: true,
    },
    email: {
        type: String,
        required: true,
        trim: true,
        lowercase: true,
        unique: true,
        validate(val) {
            if (!validator.isEmail(val)) throw new Error('Invalid Email')
        }
    },
    password: {
        type: String,
        required: true,
        minLength: 7,
        trim: true,
        // select: false,  // to not send on find requests
        validate(val) {
            if (val.toLowerCase().includes('password')) {
                throw new Error("Password can't be password")
            }
        }
    },
    tokens: [{
        token: {
            type: String,
            required: true
        }
    }],
    role: {
        type: String, 
        enum : ['beta','admin', 'user'], 
        default: 'beta' 
    },
    availableKeywords: {
        type: Number,
        default : 100
    }
    //TODO for user profile image
    // avatar: {
    //     type: Buffer
    // }
}, { collection: 'user', timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' }})

/**Virtual fields for relation with Template Collection */
userSchema.virtual('template', {
    ref: "Template",
    localField: "_id",
    foreignField: "owner"
})
/**Virtual fields for relation with Automation Collection */
userSchema.virtual('automation', {
    ref: "Automation",
    localField: "_id",
    foreignField: "owner"
})
/**Virtual fields for relation with Site Collection */
userSchema.virtual('site', {
    ref: "Site",
    localField: "_id",
    foreignField: "owner"
})
/**Method to send profile data 
 * return user Object for profile
*/
userSchema.methods.toJSON = function () {
    let user = this
    //mongoose function for creating object
    let userObject = user.toObject()
    delete userObject.password
    delete userObject.tokens
    delete userObject.__v
    return userObject
}

/**Checking user availability */
/**this is a model method */
userSchema.statics.findByCredentials = async (email, password) => {
    const user = await User.findOne({ email })
    if (!user) throw new Error('No user found for this Email')
    const isMached = await bcrypt.compare(password, user.password)
    if (!isMached) throw new Error('Wrong Password')
    return user
}

/**Methods are used for instances */
userSchema.methods.generateAuthToken = async function () {
    let user = this
    //generate jwt
    const token = jwt.sign({ _id: user._id.toString() }, config.SECRET_KEY)
    //storing token in user's data
    user.tokens = user.tokens.concat({ token })
    await user.save();
    return token
}

/**Middelware for password hashing 
pre is for before function call, (mongooseQueryr, standad function not an arrow function)
= reffering this as the data('user') which is going to be saved
= next() will assure that the process of this pre() has been done
*/
userSchema.pre('save', async function (next) {
    let user = this
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8)
    }
    next()
})

/**Pre method to remove task when user deletes its own data */
userSchema.pre('remove', async function(next){
    let user = this
    /**deleting multiple tasks using just owner
    * import Task
    */
    await Template.deleteMany({owner : user._id})
    await Automation.deleteMany({owner : user._id})
    next()
})

const User = mongoose.model("User", userSchema)

module.exports = User

// const UserName = new User({ name: "uday", age: 24, email: "reachudaysingh@gmail.com" })

// UserName.save().then((res) => console.log("data saved=============", res)).catch((e) => console.log("er============", e))