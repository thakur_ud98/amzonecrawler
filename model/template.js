const mongoose = require('mongoose')
// const validator = require('validator')
// const bcrypt = require('bcryptjs')
// const jwt = require('jsonwebtoken')

const templateSchema = new mongoose.Schema({
    templateName: {
        type: String,
        required: true,
        trim: true,
    },
    postTitle: {
        type: String,
        required: true,
        trim: true,
    },
    postContent: {
        type: String,
        required: true,
        trim: true,
    },
    templateCategory: {
        type: String,
        required: true,
        trim: true,
    },owner:{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },

}, { collection: 'template', timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' } })




const Template = mongoose.model("Template", templateSchema)

module.exports = Template