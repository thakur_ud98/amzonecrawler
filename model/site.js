const mongoose = require('mongoose')
const validator = require('validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

let siteSchema = new mongoose.Schema({
    siteName: {
        type: String,
        required: true,
        trim: true
    },
    siteUrl: {
        type: String,
        required: true,
        trim: true,
        validate(val) {
            if (!validator.isURL(val)) throw new Error('Invalid Site Url')
        }
    },
    owner:{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'User'
    },
    siteUserName: {
        type: String,
        required: true,
        trim: true,
    },
    sitePassword: {
        type: String,
        required: true,
        trim: true
    },
    amazonAff :{
        type: String,
        required: true,
        trim: true 
    },
    ebayAff: {
        type: String,
        required: true,
        trim: true
    }
}, {collection: 'site', timestamps: {createdAt: 'created_at', updatedAt: 'updated_at'}})


const Site = mongoose.model("Site", siteSchema)

module.exports = Site