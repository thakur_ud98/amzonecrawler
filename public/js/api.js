
var addTemplate = document.querySelector('#addTemplate') || ""
const clearAddTemplateForm = document.querySelector('#resetTemplate') || ""
let addTemplateObje = {
    templateName: "",
    postTitle: "",
    postContent: "",
    templateCategory: ""
}
let automationObj = {
    keywords: "",
    template: "",
    site: ""
}

let addSiteObj = {
    "siteName": "",
    "siteUrl": "",
    "siteUserName": "",
    "sitePassword": "",
    "amazonAff" :"",
    "ebayAff": ""
}


/**Method for Add tamplate 
 * quill is reffered as Html Editor
*/
try {
    if (addTemplate != "") {
        addTemplate.addEventListener('click', (e) => {
            e.preventDefault();
            Object.keys(addTemplateObje).map(v => {
                addTemplateObje[v] = v == 'postContent' ? quill.root.innerHTML.replace('&gt;', '>').replace('&lt;', '<') : document.querySelector(`#${v}`).value
            })
            document.getElementById("addTemplate").classList.add('disabled');
            fetch('/user/template/add?check=testing123', {
                method: 'POST',
                body: JSON.stringify({ 'data': addTemplateObje }),
                headers: {
                    'Content-Type': 'application/json'
                },
                redirect: 'follow'
            }).then((response) => {
                response.json().then((resp) => {
                    if (resp.error) {
                        document.getElementById("addTemplate").classList.remove('disabled');
                        return document.querySelector('#response').textContent = resp.msg
                    }
                    resetTemplate()
                    window.location.href = resp.url
                    return;

                })
            }).catch((error) => {
                console.error("error===============", error)
            })
        })
    }

    if (clearAddTemplateForm != "") {
        clearAddTemplateForm.addEventListener('click', (e) => {
            e.preventDefault()
            resetTemplate()
        })
    }

} catch (e) { }

/**Method for Reset Add tamplate values */


const resetTemplate = function () {
    Object.keys(addTemplateObje).map(v => {
        document.querySelector(`#${v}`).value = ''
    })
    document.querySelector('#response').textContent = ''
    return
}

/**
 * Method to set preview
 */
function previewTemplate() {
    let dummyObj = {
        'product_name_1': "Product Name One",
        'product_name_2': "Product Name Two",
        'product_name_3': "Product Name Three",
        'product_name_4': "Product Name Four",
        'product_name_5': "Product Name Five",
        'product_price_1': "$1000",
        'product_price_2': "$2000",
        'product_price_3': "$3000",
        'product_price_4': "$4000",
        'product_price_5': "$5000",
        'product_link_1': "https://jsfiddle.net/40zpau8v/",
        'product_link_2': "https://jsfiddle.net/40zpau8v/",
        'product_link_3': "https://jsfiddle.net/40zpau8v/",
        'product_link_4': "https://jsfiddle.net/40zpau8v/",
        'product_link_5': "https://jsfiddle.net/40zpau8v/",
        'product_image_1': "Product Image One",
        'product_image_2': "Product Image Two",
        'product_image_3': "Product Image Three",
        'product_image_4': "Product Image Four",
        'product_image_5': "Product Image Five",
        'keyword': "iPhone Covers"
    }

    let postContent = quill.root.innerHTML.replace(/&gt;/g, '>').replace(/&lt;/g, '<')
    Object.keys(dummyObj).map(v => {
        if (postContent.includes(v)) {
            postContent = postContent.replace(new RegExp('<%=' + v + '%>', 'gm'), dummyObj[v])
        }
    })
    let postTitle = document.querySelector(`#postTitle`).value.replace(/<%=keyword%>/g, dummyObj.keyword)
    let domNode = ['postTitle', 'postContent']
    domNode.map(v => {
        document.getElementById(`pre${v}`).innerHTML = v == 'postContent' ? postContent : postTitle
    })
}

function deleteById(id, type) {
    // /user/deleteById?id=5e3545f670ca73107c5bf145&&doc=template
    if(!confirm("Are sure to delete this.")) return
    try {
        fetch(`/user/deleteById?id=${id}&&doc=${type}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                response.json().then((resp) => {
                    if (resp.error) {
                        return console.log("respo=======================", resp.msg)
                    }
                    window.location.reload()
                    return console.log("response=======================", response)
                })
            })
    } catch (e) {
        console.error("e===============", e.message)
    }
}

function resetAutomation() {
    Object.keys(automationObj).map(v => document.querySelector(`#${v}`).value = v == 'keywords' ? '' : "Select")
    return;
}

function addAutomation() {
    try {
        Object.keys(automationObj).map(v => {
            automationObj[v] = document.getElementById(v).value
        })
        if (automationObj.keywords == '' || automationObj.template == "Select" || automationObj.site == "Select") return alert("Fields can't be blank.")
        document.getElementById("runAutomation").classList.add('disabled');
        fetch('/user/automation/add', {
            method: 'POST',
            body: JSON.stringify({ 'data': automationObj }),
            headers: {
                'Content-Type': 'application/json'
            },
            redirect: 'follow'
        }).then((response) => {
            response.json().then((resp) => {
                if (resp.error) {
                    document.getElementById("runAutomation").classList.remove('disabled');
                    return document.querySelector('#response').textContent = resp.msg
                }
                resetAutomation()
                window.location.href = resp.url
                return;
            })
        })
    } catch (e) {
        console.log("e=================", e)
    }

}

function resetSiteObj() {
    reset(addSiteObj)
    return
}

function addSite() {
    addSiteObj = addValue(addSiteObj)
    document.getElementById("addSiteBtn").classList.add('disabled');
    fetch('/user/sites/add', {
        method: 'POST',
        body: JSON.stringify({ 'data': addSiteObj }),
        headers: {
            'Content-Type': 'application/json'
        },
        redirect: 'follow'
    }).then((response) => {
        response.json().then((resp) => {
            if (resp.error) {
                document.getElementById("addSiteBtn").classList.remove('disabled');
                return document.querySelector('#response').textContent = resp.msg
            }
            reset(addSiteObj)
            window.location.href = resp.url
            return;

        })
    })
}

function addValue(obj) {
    Object.keys(obj).map(v => {
        obj[v] = document.querySelector(`#${v}`).value
    })
    return obj
}

function reset(obj) {
    Object.keys(obj).map(v => {
        document.querySelector(`#${v}`).value = ""
        obj[v] = ""
    })
}

function runAutomation(id = null) {
    try {
        fetch('/user/automation/run', {
            method: 'POST',
            body: JSON.stringify({ id }),
            headers: {
                'Content-Type': 'application/json'
            }
        }).then((response) => {
            response.json().then((resp) => {
                if (resp.error) {
                    return alert(resp.msg)
                    // return document.querySelector('#response').textContent = resp.msg
                }
                console.log("data===============", resp)
                return;

            })
        })
    } catch (e) {
        console.error("e===============", e)
    }
}

/**
 * Method to update Document
 * @param {*} id 
 * @param {*} task  sites | automation | template
 */
function updateById(id = null, task = null) {
    if (id == null || task == null) return alert(`Invalid operation`)
    let confirmUpdation = confirm(`Are sue to update the details!`)
    var Obj = Object.assign(task == 'sites' ? addSiteObj : task == 'template' ? addTemplateObje : null)
    if (!confirmUpdation) return;
    switch (task) {
        case 'sites':
            Obj = addValue(Obj)
            break;
        case 'template':
            Object.keys(Obj).map(v => {
                Obj[v] = v == 'postContent' ? quill.root.innerHTML.replace(/&gt;/g, '>').replace(/&lt;/g, '<') : document.querySelector(`#${v}`).value
            })
            break;
    }
    Object.keys(Obj).map(v => {
        Obj[v] == '' ? delete Obj[v] : void 0
    })
    fetch(`/user/updateById?doc=${task}`, {
        method: 'POST',
        body: JSON.stringify({ 'data': { ...Obj, ...{ id } } }),
        headers: {
            'Content-Type': 'application/json'
        },
        redirect: 'follow'
    }).then((response) => {
        response.json().then((resp) => {
            if (resp.error) {
                return document.querySelector('#response').textContent = resp.msg
            }
            window.location.href = resp.url
            return;
        })
    })
}

function logOut () {
    fetch('/user/logout', {
        method: 'POST',
    }).then((response) => {
        response.json().then((resp) => {
            if (resp.error) {
                return alert(resp.msg)
                // return document.querySelector('#response').textContent = resp.msg
            }
            localStorage.clear()
            window.location.href = "/lets-roll"

        })
    })
}