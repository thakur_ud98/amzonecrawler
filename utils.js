const axios = require('axios')
let token = "BY2fXb7eos13qIN98sbbVA"
const API = `http://api.proxycrawl.com/?token=${token}`
const WPAPI = require('wpapi')

/**
* Please note that the url should be encoded. For example, in the following url: https://www.amazon.com/sale?catId=0&SearchText=games the url should be encoded when calling the API like the following: https%3A%2F%2Fwww.amazon.com%2Fsale%3FcatId%3D0%26SearchText%3Dgames
*/

const axiosCallSerp = (keyword, scrap) => {
    let amazon_url = encodeURIComponent(`https://www.amazon.com/s?k=${keyword}`);
    return new Promise((resolve, reject) => {
        axios.get(`${API}&scraper=${scrap}&url=${amazon_url}`)
            .then(response => {
                return resolve(response.data)
            })
            .catch(error => {
                return reject(error.message)
            })
    })
}

//   scraper=amazon-serp
const amazon_serp = async (keyword) => {
    let data = []
    // for (let key of keyword) {
    await axiosCallSerp(keyword, 'amazon-serp').then(async (list) => {
        let obj = {
            // response: list,
            list: list.body.products.slice(0, 5), //limiting to 5 producst
            keyword: keyword
        }
        // for(var i = 0;i < 5; i ++ ) {
        //    obj.list[i].details = await amazon_product_details(obj.list[i])
        // }
        data.push(obj)
    })
    // }
    console.log('Done');
    return data
}

const axiosCallProductDetail = (key, asin) => {
    return new Promise((resolve, reject) => {
        console.log("")
        let amazon_url = encodeURIComponent(`https://www.amazon.com/${key}/dp/${asin}`)
        axios.get(`${API}&scraper=amazon-product-details&url=${amazon_url}`)
            .then(response => {
                return resolve(response.data.body)
            })
            .catch(error => {
                return reject(error.message)
            })
    })
}

// const &scraper=amazon-product-details
const amazon_product_details = async (product) => {
    let data = []
    // for (let p of list) {
    await axiosCallProductDetail(product.name, product.asin).then((response) => {
        data.push(response)
    })
    // }
    console.log('Done');
    return data
}


const createContent = async (template, list, keyword) => {
    let dynamicContent = Object.assign({}, template)
    dynamicContent.postTitle = await dynamicContent.postTitle.replace(/<%=keyword%>/g, keyword)
    dynamicContent.postContent = await dynamicContent.postContent.replace(/&gt;/g, '>').replace(/&lt;/g, '<').replace(/%3C/g, '<').replace(/%3E/g, '>')
    dynamicContent.postContent = await dynamicContent.postContent.replace(/<%=keyword%>/g, keyword)
    for (let [i, l] of list.entries()) {
        let v = i + 1
        dynamicContent.postContent = await dynamicContent.postContent.replace(new RegExp('<%=product_name_' + v + '%>', 'gm'), l.name)
        dynamicContent.postContent = await dynamicContent.postContent.replace(new RegExp('<%=product_price_' + v + '%>', 'gm'), l.price)
        dynamicContent.postContent = await dynamicContent.postContent.replace(new RegExp('<%=product_link_' + v + '%>', 'gm'), l.url)
        dynamicContent.postContent = await dynamicContent.postContent.replace(new RegExp('<%=product_image_' + v + '%>', 'gm'), l.image)
    }
    // console.log("dynamicContent======================",dynamicContent)
    // Promise.all([content, list, dynamicContent]).then(v=>{
    return { template, list, dynamicContent };
    // })
}

const publishToWp = async (site, content) => {
    var wp = new WPAPI({
        // endpoint: 'https://trendingcelebs.wiki/wp-json/', //  site.siteUrl+/wp-json/
        // // This assumes you are using basic auth, as described further below
        // username: 'jagdish', //site.siteUserName
        // password: 'Viis@0000' //site.sitePassword
        endpoint: (site.siteUrl.slice(-1) == "/" ? site.siteUrl.slice(0, -1) : site.siteUrl  ) +"/wp-json/",
        username: site.siteUserName,
        password: site.sitePassword,
    });
    try {
        return new Promise((resolve, reject) => {
            wp.posts().create({
                title: content.postTitle,
                content: content.postContent,
                status: 'draft'
            })
                .then(response => {
                    return resolve(response)
                })
                .catch(error => {
                    return reject(error.message)
                })
        })
    } catch (e) {
        throw new Error(e.message)
    }
}



module.exports = { amazon_serp, amazon_product_details, createContent, publishToWp}