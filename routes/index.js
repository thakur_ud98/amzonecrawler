var express = require('express');
var router = express.Router();
const User = require('../model/user')
const products = require('../Response/serp').products
const util = require('../utils')
const msg = require("../messages")

let token = "BY2fXb7eos13qIN98sbbVA"
const API = `http://api.proxycrawl.com/?token=${token}`

/* 
  render Keyword input page
*/
router.get('/', (req, res) => {
  res.render('index', { title: "Amazon data" })
})

/**
 * API to crawle products on th ebase of Keywords
 */
router.get('/productList', async function (req, res, next) {
  try {
    let keyword = req.query.keyword || ""
    if (keyword == "") {
      return res.status(200).json({
        success: false,
        data: null,
        msg: "Please provide 1 or more(with comma) keywords"
      })
    }
    keyword = keyword.split(",")
    let TotalKeywords = keyword.length
    let data = []
    data = await util.amazon_serp(keyword)
    // data = [{response, list, keyword}, {response, list, keyword}]
    if (data.length != 0) {
      res.render('productList', { data, title: "List" })
      // res.send({ data})
    }
  } catch (e) {
    res.status(500).json({
      success: false,
      data: null,
      msg: e.message
    })
  }
});

/*
API to render product List page
*/
router.get('/dummy_list', (req, res) => {
  res.render('productList', { data: products, title: "List" })
})

/**
 * API for product details
 * &scraper=amazon-product-details
 */
// /productDetails?asin=B07S5QWM6L
router.get('/productDetailsWithKeyAndAsin', async (req, res) => {
  try {
    let { key, asin } = req.query
    let data = await util.amazon_product_details(key, asin)
    res.send({ data })
  } catch (e) {
    return res.status(500).json({
      success: false,
      data: null,
      msg: e.message
    })
  }
})

router.get('/detailsForEachProduct', async (req, res) => {
  // products
  try {
    let data = []
    data = await util.amazon_product_details(products[0].list)
    res.send({ data })
  } catch (e) {
    return res.status(500).json({
      success: false,
      data: null,
      msg: e.message
    })
  }
})

router.get('/lets-roll', async (req, res) => {
  res.render('signup')
})

router.post('/signup', async (req, res) => {
  try{
    let data = req.body.data
    //create user in DB
    //email, password, name, age
    const user = User(data)
    await user.save()
    return res.status(200)
              .send({"msg":"User created", "data": null,"error" : false, url: '/lets-roll'})
  }catch(e){
    res.status(500).send({"msg": e.message, error: true, data: null})
  }
})

router.get('/login', async (req, res) => {
  res.render('login')
})

router.post('/login', async (req, res) => {
try{
  let data = req.body.data
  let logInObj = Object.keys(data)
  let allowedObj = ['email', 'password']
  const isValidRequest = logInObj.every((val)=> allowedObj.includes(val))
  if (!isValidRequest) return res.status(400).send({ msg: "Bad request format", data: null, error: true });

  /**This method can be use for all, thats why we have used User */
  const user = await User.findByCredentials(data.email, data.password)
  const token = await user.generateAuthToken()
  return res.status(200)
            .cookie('token', `Bearer ${token}`, {
              expires: new Date(Date.now() + 8 * 3600000) // cookie will be removed after 8 hours
            })
            .send({ msg: msg.LOGGED_IN, data: {user: {'name' : user.name, }, token}, url : "/user/dashboard", error: false });
    }catch(e){
      res.send({
        error: true,
        data: null,
        msg: e.message
      })
    }
})

module.exports = router;
