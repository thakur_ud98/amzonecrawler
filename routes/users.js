const router = require('express').Router()
const msg = require('../messages')
const dummyObj = require('../Response/serp')
const Template = require('../model/template')
const Automation = require('../model/automation')
const Site = require('../model/site')
const utils = require('../utils')

router.get('/', function (req, res) {
  res.render('./User/index', { title: 'User Page' })
});

//TODO make it post
router.get('/profile', function (req, res) {
  res.render('./User/profile', { title: 'Profile Page' })
})

/**************Template routes***************/

/* GET template Listing listing. */
router.get('/template', async function (req, res) {
  try {
    //TODO handle sorting and other operations
    let match = {}
    let options = {}
    await req.user.populate('template').execPopulate()
    // res.json(templates)
    res.render('./Templates/index', { title: 'Template Page', list: req.user.template })
  } catch (e) {
    console.error("e=================", e)
  }
});

router.get('/template/add', function (req, res) {
  res.render('./Templates/addTemplate', { title: 'Add Template Page' })
})

router.post('/template/add', async function (req, res) {
  try {
    let data = req.body.data
    data.postContent = data.postContent.replace(/&gt;/g, '>').replace(/&lt;/g, '<')
    let template = await Template({
      ...req.body.data,
      owner: req.user._id
    })
    await template.save()
    res.json({
      data,
      msg: msg.TEMPLATE_ADD,
      error: false,
      url: '/user/template'
    })
  } catch (e) {
    console.error("e============", e)
  }
})

router.get('/template/preview', async function (req, res) {
  try {
    let id = req.query.id
    let template = await Template.findById(id)
    let product = dummyObj.products
    let data = await utils.createContent(template.toObject(), product[0].list, product[0].keyword)
    res.render('./Templates/previewTemplate', { title: 'Dynamic Template Page', Obj: data.dynamicContent })
    // res.json({
    //   data: {
    //     template,
    //     newtemplate,
    //     keyword: product[0].keyword,
    //     list
    //   },
    //   msg: null,
    //   error: false
    // })
  } catch (e) {
    res.status(500).json({
      data: null,
      msg: e.message,
      error: true
    })
  }
})

router.get('/template/edit', async function (req, res) {
  try {
    let id = req.query.id || ""
    let template = await Template.findById(id).select(['templateName', 'postTitle', 'postContent', 'templateCategory'])
    if (!template) return res.json({ data: null, msg: "Site not found", error: true })
    return res.render('./Templates/editTemplate', { title: 'Edit Template Page', template })
  } catch (e) {
    res.json({
      data: null,
      msg: e.message,
      error: true
    })
  }
})

/**************SITE routes*******************/
router.get('/sites', async function (req, res) {
  try {
    await req.user.populate('site', 'siteName siteUrl').execPopulate()
    res.render('./Sites/index', { title: 'Site Page', list: req.user.site })
    // return res.json({
    //   sites,
    //   msg: '',
    //   error: false
    // })
  } catch (e) {
    return res.status(500).json({
      msg: e.message,
      error: true
    })
  }
});

router.get('/sites/add', function (req, res) {
  res.render('./Sites/addSite', { title: 'Add Site Page' })
})

router.post('/sites/add', async function (req, res) {
  try {
    let site = await Site({
      ...req.body.data,
      owner: req.user._id
    })
    await site.save()
    return res.json({
      data: site,
      msg: msg.SITE_ADD,
      error: false,
      url: '/user/sites'
    })
  } catch (e) {
    res.json({
      data: null,
      msg: e.message,
      error: true
    })
  }
})

router.get('/sites/edit', async function (req, res) {
  try {
    let id = req.query.id
    let site = await Site.findById(id).select(['siteName', 'siteUrl', 'siteUserName'])
    if (!!site) return res.render('./Sites/editSite', { title: 'Edit Site', site })
    return res.json({ data: null, msg: "Site not found", error: true })
  } catch (e) {
    res.json({
      data: null,
      msg: e.message,
      error: true
    })
  }
})


/**************Automation routes*************/
router.get('/automation', async function (req, res) {
  try {
    // let automation = await Automation.aggregate([
    //   { $match: {} },
    //   { $project: { keywords: '$keywords', status: '$status', templateName: '$template.templateName', site: '$site.siteName', created_at: '$created_at', pushed: '$pushed' } }
    // ])
    await req.user.populate({
      path: 'automation',
      $match: {},
      $project: { keywords: '$keywords', status: '$status', templateName: '$template.templateName', site: '$site.siteName', created_at: '$created_at', pushed: '$pushed' }
    }).execPopulate()
    console.log("automation=================", req.user.automation)
    return res.render('./Automation/index', { title: 'Automation Page', automation: req.user.automation })
  } catch (e) {
    console.log("e==============", e)
    res.send({
      data: null,
      msg: e.message
    })
  }
});

router.get('/automation/add', async function (req, res) {
  try {
    await req.user.populate('site', 'siteName').execPopulate()
    await req.user.populate('template', 'templateName').execPopulate()
    res.render('./Automation/addAutomation', { title: 'Add Automation Page', tempList : req.user.template, sites : req.user.site })
  } catch (e) {

  }
})

router.post('/automation/add', async function (req, res) {
  try {
    let data = req.body.data
    await req.user.populate('template').execPopulate()
    await req.user.populate('site').execPopulate()
    let site = req.user.site.find(s => s._id == data.site)
    let template = req.user.template.find(t => t._id == data.template)
    data.template = {
      postTitle: template.postTitle,
      templateName: template.templateName,
      postContent: template.postContent,
      templateCategory: template.templateCategory
    }
    data.site = {
      siteName: site.siteName,
      siteUrl: site.siteUrl,
      siteUserName: site.siteUserName,
      sitePassword: site.sitePassword,
    }
    let automation = new Automation({...data, owner : req.user._id})
    await automation.save()
    return res.json({
      data : automation,
      msg: msg.AUTOMATION_ADD,
      error: false,
      url: '/user/automation'
    })
    // await req.user.populate({
    //   path: 'template',
    //   $match: {},
    //   $project: { keywords: '$keywords', status: '$status', templateName: '$template.templateName', site: '$site.siteName', created_at: '$created_at', pushed: '$pushed' }
    // }).execPopulate()
    
    // let automation = await Automation.create(data)
    // // console.log("automation===================",automation)
    // res.json({
    //   data: automation,
    //   msg: msg.AUTOMATION_ADD,
    //   error: false,
    //   url: '/user/automation'
    // })
  } catch (e) {
    res.json({
      data: null,
      msg: e.message,
      error: true
    })
  }
})

router.post('/automation/run', async function (req, res) {
  try {
    let id = req.body.id

    let finalData = []

    //get automation:Done
    let automation = await Automation.findById(id)
    //use keywords and get data from crawler:Done
    let keywords = automation.keywords.split(",")
    //create posts nad push to an array:Done
    for (key of keywords) {
      let dataByKeyword = await utils.amazon_serp(key)
      let data = await utils.createContent(automation.template, dataByKeyword[0].list, key)
      data.wpResponse = await utils.publishToWp(automation.site, data.dynamicContent)
      Promise.all([dataByKeyword, data]).then(function (values) {
        finalData.push({ data })
      })
    }
    //loop array nad push posts to WP

    //return resp :Done
    Promise.all([automation, finalData]).then(function (values) {
      res.status(200).json({
        data: values,
        msg: msg.AUTOMATION_RUN,
        error: false,
        url: '/user/automation'
      })
    });

  } catch (e) {
    console.log("e========================", e)
    res.send({
      data: null,
      msg: e.message || e,
      error: true
    })
  }
})
/**************Automation routes*************/


/*************User Routes******************/
router.get('/dashboard', async function (req, res) {
  try {
    //TODO get credits from user Document
    await req.user.populate({
      path: 'automation',
      $match: {},
      $project: { keywords: '$keywords', status: '$status', templateName: '$template.templateName', site: '$site.siteName', created_at: '$created_at', pushed: '$pushed' }
    }).execPopulate()
    // console.log("user====================", req.user)
    // let autoCount = req.user.automation.length

    // let automation = await Automation.aggregate([
    //   { $match: {} },
    //   { $project: { keywords: '$keywords', status: '$status', templateName: '$template.templateName', site: '$site.siteName', created_at: '$created_at', pushed: '$pushed' } }
    // ])
    let pushedCount = await Automation.aggregate([
      { $match: { 'pushed': true } }
    ])
    // let keywordCount = req.user.availableKeywords
    // await Automation.find({}, ['keywords'])
    // keywordCount = keywordCount.reduce((a, v) => {
    //   a = v.keywords + " ," + a
    //   return a
    // }, "")
    // keywordCount = keywordCount.slice(0, -2).split(" ,").length
    return res.render('./User/dashboard', { title: 'Dashboard', autoCount : req.user.automation.length, keywordCount : 0, pushedCount: pushedCount.length, automation : req.user.automation, credit : req.user.availableKeywords })
  } catch (e) {
    res.send({
      data: null,
      msg: e.message,
      error: true
    })
  }
})
/**API for logout user single session
 * @param Object
 * @returns Object
*/
router.post("/logout", async (req, res)=>{
  try{
      req.user.tokens = req.user.tokens.filter((token)=>  token.token !== req.token )
      await req.user.save()
      return res.clearCookie("token").send({ msg: msg.LOG_OUT, data: null, error: false });
  }catch(e){
      return res.status(500).send({ msg: e.toString(), data: null, error: true });
  }
})

/**API for logout user from all session
* @param Object
* @returns Object
*/
router.post("/logoutAll", async (req, res)=>{
  try{
      req.user.tokens = []
      await req.user.save()
      return res.clearCookie("token").send({ msg: msg.LOG_OUT, data: null, error: false });
  }catch(e){
      return res.status(500).send({ msg: e.toString(), data: null, error: true });
  }
})


/*************Common API *******************/

router.post('/deleteById', async function (req, res) {
  try {
    let id = req.query.id || ""
    let doc = req.query.doc || ""
    let deletedDoc;
    switch (doc) {
      case 'template':
        deletedDoc = await Template.findByIdAndDelete(id)
        break;
      case 'site':
        deletedDoc = await Site.findByIdAndDelete(id)
        break;
    }
    console.log("deletedDoc===============", deletedDoc)
    if (!deletedDoc) {
      return res.json({
        data: null,
        msg: "Document not found",
        error: true
      })
    }
    return res.json({
      data: {
        id,
        doc
      },
      msg: "Delete done!",
      error: false
    })
  } catch (e) {
    res.status(500).json({
      data: null,
      msg: e.message,
      error: true
    })
  }
})

router.post('/updateById', async function (req, res) {
  try {
    let data = req.body.data
    let doc = req.query.doc || ""
    let updatedDoc;
    switch (doc) {
      case 'template':
        updatedDoc = await Template.findByIdAndUpdate(data.id, { $set: data }, { new: true })
        break;
      case 'sites':
        updatedDoc = await Site.findByIdAndUpdate(data.id, { $set: data }, { new: true })
        break;
    }
    if (!updatedDoc) return res.status(404).send({ msg: "No record found for this id", data: null, error: true });
    res.json({
      msg: "data has been updated",
      url: `/user/${doc}`,
      error: false
    })
  } catch (e) {
    res.json({
      data: null,
      msg: e.message,
      error: true
    })
  }
})


module.exports = router;