const msg = {
     TEMPLATE_ADD : "Template Added successfully",
     AUTOMATION_ADD : "Autoamtion Added successfully",
     AUTOMATION_RUN : "Autoamtion Done successfully",
     SITE_ADD : "Site Added successfully",
     USER_CREATED : "User created Successfully",
     LOGGED_IN : "Logged in Successfully",
     LOG_OUT : "Logged out successfully",

}

module.exports = msg