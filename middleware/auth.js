const jwt = require('jsonwebtoken')
const User = require('../model/user')
const config = require('../config')
const auth = async (req, res, next) => {
    try{
        // let token = req.header('Authorization').replace("Bearer ","")
        let token = req.cookies.token.replace("Bearer ","")
        if(!token) return res.status(401).send({"msg":"Please login first", "data": null,"error" : true})
        let decode = jwt.verify(token, config.SECRET_KEY)
        if(decode._id === undefined) return res.status(401).send({"msg":"Invalid token", "data": null,"error" : true})
        const user = await User.findOne({_id: decode._id, 'tokens.token': token})
        if(!user) throw new Error()
        req.token = token
        req.user = await user
        next()
    } catch(e){
        res.redirect('/lets-roll')
        // return
        // res.status(200).send({"msg":"Please Authenticate", "data": e,"error" : true})
    }
    
}


module.exports = auth